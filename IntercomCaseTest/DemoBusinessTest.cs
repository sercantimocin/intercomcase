﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntercomCase.Business;
using IntercomCase.Core;
using IntercomCase.Model;
using Xunit;

namespace IntercomCaseTest
{
    public class DemoBusinessTest
    {
        private IDemoBusiness _demoBusiness;

        public DemoBusinessTest()
        {
            _demoBusiness = new DemoBusiness(new MathHelper());
        }

        [Fact]
        public void Get_AllCustomers()
        {
            var sut = _demoBusiness.GetCustomerList(GlobalVariables.EndPointCustomers);

            Assert.NotEmpty(sut);
            Assert.Equal(32, sut.Count);
        }

        [Fact]
        public void Calc_DistanceGreenwichAndNorthPole_ReturnBelow214dot2355km()
        {
            //Greenwich
            var loc1 = new Location(51.47879, -0.010677);
            //NorthPole
            var loc2 = new Location(89.999999, 0);

            var sut = _demoBusiness.CalculateDistance(loc1, loc2);

            var diff = Math.Abs(sut - 4284.71);

            //Assert written by Standard deviation 0.05+-
            Assert.True(diff <= 4284.71*0.05);
        }

        [Fact]
        public void Filter_OneCustomerInRange_ReturnValid()
        {
            var customers = new List<Customer>();
            customers.Add(new Customer(1, "test1", 52.986375, -6.043701));
            customers.Add(new Customer(2, "test2", 21, 25));

            var results = _demoBusiness.FilterCustomersNearByOffice(customers, GlobalVariables.DublinOfficeLocation, 50);

            Assert.Single(results);
            Assert.Equal("test1", results.First().Value.Name);
        }

        [Fact]
        public void Filter_NoCustomerInRange_ReturnEmpty()
        {
            var customers = new List<Customer>();
            customers.Add(new Customer(2, "test2", 21, 25));

            var results = _demoBusiness.FilterCustomersNearByOffice(customers, GlobalVariables.DublinOfficeLocation, 50);

            Assert.Empty(results);
        }
    }
}

using IntercomCase.Core;
using Xunit;

namespace IntercomCaseTest
{
    public class MathHelperTest
    {
        private MathHelper _mathHelper;

        public MathHelperTest()
        {
            _mathHelper = new MathHelper();
        }

        [Fact]
        public void Convert_10DegreeToRadian_0dot17453292519943295()
        {
            var sut = _mathHelper.DegreeToRadian(10);

            Assert.Equal(0.17453292519943295, sut);
        }

        [Fact]
        public void Calc_1DegreeToArccos_0()
        {
            var sut = _mathHelper.Arccos(1);

            Assert.Equal(0, sut);
        }

        [Fact]
        public void Calc_2DegreeToArccos_NaN()
        {
            var sut = _mathHelper.Arccos(2);

            Assert.Equal(double.NaN, sut);
        }
    }
}


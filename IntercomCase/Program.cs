﻿using System;
using System.Collections.Generic;
using System.IO;
using IntercomCase.Business;
using IntercomCase.Core;
using IntercomCase.Model;
using Microsoft.Extensions.DependencyInjection;

namespace IntercomCase
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            RegisterServices();
            var service = _serviceProvider.GetService<IDemoBusiness>();

            List<Customer> customers = service.GetCustomerList(GlobalVariables.EndPointCustomers);
            SortedDictionary<int, Customer> nearByCustomers = service.FilterCustomersNearByOffice(customers, GlobalVariables.DublinOfficeLocation, GlobalVariables.MaxValidDistance);
            WriteCustomersToFile(nearByCustomers, service);

            DisposeServices();
            Console.ReadKey();
        }

        private static void WriteCustomersToFile(SortedDictionary<int, Customer> nearByCustomers, IDemoBusiness service)
        {
            string directory = Path.Combine(Environment.CurrentDirectory, "output.txt");
            using (StreamWriter outputFile = new StreamWriter(directory))
            {
                foreach (var customer in nearByCustomers)
                {
                    outputFile.WriteLine("Customer Id : {0} Distance : {1}", customer.Key, service.CalculateDistance(customer.Value.Location, GlobalVariables.DublinOfficeLocation));
                }
            }
        }

        private static void RegisterServices()
        {
            var collection = new ServiceCollection();
            collection.AddSingleton<IMathHelper, MathHelper>();
            collection.AddScoped<IDemoBusiness, DemoBusiness>();
            _serviceProvider = collection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}

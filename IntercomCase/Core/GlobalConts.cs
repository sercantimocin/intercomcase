﻿using IntercomCase.Model;

namespace IntercomCase.Core
{
    public static class GlobalVariables
    {
        public static string EndPointCustomers = "https://s3.amazonaws.com/intercom-take-home-test/customers.txt";
        public static Location DublinOfficeLocation = new Location(53.339428, -6.257664);
        public static double MaxValidDistance = 100;
    }
}

﻿using System;
using IntercomCase.Model;

namespace IntercomCase.Core
{
    public interface IMathHelper
    {
        double Arccos(double x);
        double DegreeToRadian(double angle);
    }

    public class MathHelper : IMathHelper
    {
        public static double EarthRadiusKm = 6399.594;
        public static double StraightAngle = 180.0;

        public double Arccos(double x)
        {
            return Math.Atan(-x / Math.Sqrt(-x * x + 1)) + 2 * Math.Atan(1);
        }

        public double DegreeToRadian(double angle)
        {
            return Math.PI * angle / StraightAngle;
        }
    }
}

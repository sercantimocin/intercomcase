﻿using System;
using IntercomCase.Core;

namespace IntercomCase.Model
{
    public class Customer
    {
        public Customer(int id, string name, double latitude, double longitude)
        {
            Id = id;
            Name = name;
            Location = new Location(latitude, longitude);
        }

        public int Id { get; }
        public string Name { get; }
        public Location Location { get; }
    }
}
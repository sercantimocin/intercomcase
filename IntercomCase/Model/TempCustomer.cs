﻿using Newtonsoft.Json;

namespace IntercomCase.Model
{
    public class TempCustomer
    {
        [JsonProperty("user_id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("latitude")]
        public double Latitude { get; set; }
        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using IntercomCase.Core;
using IntercomCase.Model;
using Newtonsoft.Json;

namespace IntercomCase.Business
{
    public interface IDemoBusiness
    {
        double CalculateDistance(Location firstLocation, Location secondLocation);
        List<Customer> GetCustomerList(string endPointCustomers);
        SortedDictionary<int, Customer> FilterCustomersNearByOffice(List<Customer> customers, Location officeLocation, double rangeKm);
    }

    public class DemoBusiness : IDemoBusiness
    {
        private readonly IMathHelper _mathHelper;

        public DemoBusiness(IMathHelper mathHelper)
        {
            _mathHelper = mathHelper;
        }

        public SortedDictionary<int, Customer> FilterCustomersNearByOffice(List<Customer> customers, Location officeLocation, double rangeKm)
        {
            var customersNearByOffice = new SortedDictionary<int, Customer>();

            foreach (var customer in customers)
            {
                var distance = this.CalculateDistance(customer.Location, officeLocation);
                if (distance > 0 && distance < rangeKm)
                {
                    customersNearByOffice.Add(customer.Id, customer);
                }
            }

            return customersNearByOffice;
        }

        public double CalculateDistance(Location firstLocation, Location secondLocation)
        {
            double lat1Radian = _mathHelper.DegreeToRadian(firstLocation.Latitude);
            double long1Radian = _mathHelper.DegreeToRadian(firstLocation.Longitude);
            double lat2Radian = _mathHelper.DegreeToRadian(secondLocation.Latitude);
            double long2Radian = _mathHelper.DegreeToRadian(secondLocation.Longitude);

            double absoluteDiffLongitude = Math.Abs(long1Radian - long2Radian);
            double s1 = Math.Sin(lat1Radian) * Math.Sin(lat2Radian);
            double s2 = Math.Cos(lat1Radian) * Math.Cos(lat2Radian) * Math.Cos(absoluteDiffLongitude);

            double centralAngle = _mathHelper.Arccos(s1 + s2);
            return centralAngle * MathHelper.EarthRadiusKm;
        }

        public List<Customer> GetCustomerList(string endPointCustomers)
        {
            List<Customer> customers = new List<Customer>();

            using (var wc = new System.Net.WebClient())
            {
                string contents = wc.DownloadString(endPointCustomers);
                var customersJsonArr = contents.Split("\n");

                foreach (var customerJson in customersJsonArr)
                {
                    TempCustomer tempCus = JsonConvert.DeserializeObject<TempCustomer>(customerJson);
                    Customer newCustomer = new Customer(tempCus.Id, tempCus.Name, tempCus.Latitude, tempCus.Longitude);
                    customers.Add(newCustomer);
                }

                return customers;
            }
        }
    }
}
